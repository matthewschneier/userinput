from django.forms import ModelForm
from .models import UserInfo

class UserForm(ModelForm):
    class Meta:
        model = UserInfo
        fields = [
            "first", "middle", "last", "address1", "address2", "phone"
        ]
