from django.contrib import messages
from django.shortcuts import render
from .forms import UserForm
from .models import *


def home(request):
    users = UserInfo.objects.all()
    form = UserForm()
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            first = form.cleaned_data["first"]
            middle = form.cleaned_data["middle"]
            last = form.cleaned_data["last"]
            address1 = form.cleaned_data["address1"]
            address2 = form.cleaned_data["address2"]
            phone = form.cleaned_data["phone"]
            user = UserInfo(
                first=first, middle=middle, last=last,
                address1=address1, address2=address2, phone=phone
            )
            user.save()
            messages.info(request, "User added.")
            return render(request, "home.html", {
                "form": form,
                "numUsers": len(users)
            })
    return render(request, "home.html", {
        "form": form,
        "numUsers": len(users),
    })


def viewUsers(request):
    users = UserInfo.objects.all()
    return render(request, "users.html", {"users": users})
