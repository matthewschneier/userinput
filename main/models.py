from django.db import models
from phone_field import PhoneField

# Create your models here.

class UserInfo(models.Model):
    first = models.CharField("First Name", max_length=99)
    middle = models.CharField(
        "Middle Name", blank=True, null=True, max_length=99
    )
    last = models.CharField("Last Name", max_length=99)
    address1 = models.CharField("Address Line 1", max_length=99)
    address2 = models.CharField(
        "Address Line 2", blank=True, null=True, max_length=99
    )
    phone = PhoneField("Contact Phone Number", blank=True, null=True)
