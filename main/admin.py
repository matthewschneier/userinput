from django.contrib import admin
from .models import *

# Register your models here.
admin.site.site_header = "UserInput Admin"
admin.site.site_title = "UserInput Admin Area"
admin.site.index_title = "Welcome to the UserInput Admin Area"
admin.site.register(UserInfo)
