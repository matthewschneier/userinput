from django.conf.urls import url
from django.urls import path
from . import views


app_name = "userInput"

urlpatterns = [
    path("", views.home, name="home"),
    url("home/", views.home, name="home"),
    url("users/", views.viewUsers, name="viewUsers"),
]
