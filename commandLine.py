import django
django.setup()
from userInput import settings
from main.models import UserInfo

def main():
    users = UserInfo.objects.all()
    print(f"Number of users in db - {len(users)}")
    print("Add new user.")
    first = getInfo(message="First Name")
    middle = getInfo(required=False, message="Middle Name")
    last = getInfo(message="Last Name")
    address1 = getInfo(message="Address Line 1")
    address2 = getInfo(required=False, message="Address Line 2")
    phone = getInfo(required=False, message="Phone Number")
    user = UserInfo(
        first=first, middle=middle, last=last,
        address1=address1, address2=address2, phone=phone
    )
    user.save()
    return "User added."

def getInfo(
        required=True, maxLength=99, message=""
    ):
    info = input(f"{message} (required): ") if \
        required else input(f"{message} (optional): ")
    while (not info and required) or len(info) > maxLength:
        info = input(f"{message} (required): ") if \
            required else input(f"{message} (optional): ")
    return info


if __name__ == "__main__":
    print(main())
