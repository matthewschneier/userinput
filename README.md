# UserInput

## Setup

1. Download repo.
2. Setup virutal env.
```
python3 -m venv {virtual env name}
```
3. Activate virtual env.
```
. {virtual env name}/bin/activate
```
4. Install requirements.
```
pip install -r requirements.txt
```
5. Set settings env var.
```
export DJANGO_SETTINGS_MODULE=userInput.settings
```
6. Set superuser
```
python manage.py createsuperuser
```

## Command Line
To run command line script to add user:
```
python commandLine.py
```
### To add new field
1. Add field to main/models.py
2. python manage.py makemigrations
3. python migrate
4. Add another getInfo() call in commandLine.py in main()

## Web Interface
To run webserver:
```
python manage.py runserver
```
1. Go to http://127.0.0.1:8000/home
2. Enter in user info.
3. Submit form.
There is also a page called "View Users" to view all users in db.

## Current requirements
- Whether the app is run in through the web interface or the command line the user info is saved to a local file (db.sqlite3).
- Both ways display the current number of users in the db.
- Concurrent users are not a problem.

## Future requirements
- Web interface can act as the GUI.
- Additonal fields can be added easily by modifying main/models.py
- Constraints are easy to add to model fields.
- Rules are easy to change in the models file.
- Change to remote db can be done by modifying userInput/settings.py in the DATABASES section.
